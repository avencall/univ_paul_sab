#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# DATE 17/05/2021
# AUTHOR : Jean-Baptiste MARIN

from xivo_agid import call_rights
import common
from xivo import anysql
from xivo.BackSQL import backpostgresql
from xivo.agi import *
import sys
import psycopg2


agi = AGI()
#agi.verbose("COUCOU")
# DEFINITION DES DROITS D APPEL
# un droit INTERDIT comportant les numéros interdits
# un droit PRIVILEGE contenant les utilisateurs privilegies
DROITS_INTERDITS = 'numero-interdits'
DROITS_PRIVILEGE = 'privileges_national'

# on positione  un droit d'appel à FALSE et PRIVILEGE d'appel à FALSE qui viendra se jouer en sous-routine d'appel sortant
IS_EXTEN_FORBIDDEN = "0"
IS_USER_PRIVILEGE = "0"

#DB_URI = 'postgresql://asterisk:proformatique@localhost:5432/asterisk'


def db_cnt():
    conn = psycopg2.connect(host="localhost",database="asterisk",user="asterisk",password="proformatique")
    return conn

def db_connect(db_uri):
    return anysql.connect_by_uri(db_uri)



def ups_numeros_interdits(cursor):
    try:
        # on recupere le numéro appele
        dst_num = agi.get_variable('XIVO_DSTNUM')
        #dst_num = "0160740001"
        # on recupere l'ID de l'utilisateur
        #user_id= "1191"
        user_id = agi.get_variable('XIVO_USERID')

        agi.verbose("### EXECUTING QUERY TO KNOW IF NUMBER IS FORBIDDEN OR USER IS PRIVILEGE  WITH ARG DST_NUM = %s and USER_ID = %s ###" % (dst_num, user_id))

        # on test si l'extension fait partie des numéros interdits
        if(is_exten_contained_in_rightcall(cursor, dst_num, DROITS_INTERDITS)):
            #print("c'est interdit")
            agi.set_variable('IS_EXTEN_FORBIDDEN', 1)
        # on test si l'utilisateur est a le droit privilege
        if(is_user_contained_in_rightcall(cursor, user_id, DROITS_PRIVILEGE)):
            #print("utilisateur privilegie")
            agi.set_variable('IS_USER_PRIVILEGE', 1)

    except:
        #print("erreur")
        agi.verbose("### XIVO_DSTNUM ou USERID manquant")


def is_user_contained_in_rightcall(cursor, user_id, rightcall_name):
    #print("Lunching")
    agi.verbose("Launching request user privilege")
    psql_query = "SELECT typeval FROM rightcallmember rm, rightcall r where rm.type = 'user' and rm.rightcallid = r.id and r.name = '%s' ;" % (rightcall_name)
    #print(psql_query)
    cursor.execute(psql_query)
    res = cursor.fetchall()
    for row in res:
        #agi.verbose(row)
        if user_id == row[0]:
            return True
    return False

def is_exten_contained_in_rightcall(cursor, dstnum, rightcall_name):
    agi.verbose("Launching request numeros interdits")
    psql_query = "SELECT exten FROM rightcall r JOIN rightcallexten re ON r.id=re.rightcallid WHERE r.name= '%s' ;" % (rightcall_name)
    #print psql_query
    cursor.execute(psql_query)
    res = cursor.fetchall()
    for row in res:
        if call_rights.extension_matches(dstnum, row[0]):
            return True
    return False

def main():
    #agi = AGI()
    agi.verbose("### BEGIN AGI ###")
    connexion = db_cnt()
    #connexion = db_connect(DB_URI)
    cursor = connexion.cursor()
    ups_numeros_interdits(cursor)
    cursor.close()
    connexion.close()
    #print("debut de l'AGI")
    #common.with_agi_params(ups_numeros_interdits)
main()

