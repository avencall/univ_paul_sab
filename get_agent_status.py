#!/usr/bin/python
# -*- coding: UTF-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your OPTION) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
__version__   = '1.0'
__date__      = '20200521'
__author__    = 'Jean-Baptiste MARIN'

import requests
import json
from pprint import pprint
import os
import csv
import psycopg2
import sys
import time
from xivo_auth_client import Client as Auth
from xivo_agentd_client import Client

LOGIN_USER = "devspe"
MDP_USER = "devspe"
AUTH_TOKEN = ""

requests.packages.urllib3.disable_warnings()
session = requests.Session()
session.verify = False
session.headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}


def _get_auth_token(LOGIN_USER,MDP_USER):
    ip_xivo = "127.0.0.1"
    #auth_url = "https://127.0.0.1:9497/0.1/token"
    auth_src = Auth(ip_xivo, username=LOGIN_USER, password=MDP_USER, verify_certificate=False)
    token_data_src = auth_src.token.new('xivo_service',expiration=60)
    token_src = token_data_src['token']
    return(token_src)

def _is_agent_logged_in(dst_num,token, agi):
    c = Client('agentd.example.com')
    agent_status = c.agents.get_agent_status_by_number('20500')
    #agent_url = "https://localhost:9493/1.0/agents/by-number/"
    #result = requests.get('%s/%s' % (agent_url, dst_num), verify=False,
    #                     headers={'Accept': 'application/json'}, timeout=2)
    if result.status_code == 404:
        agi.verbose("Agent Not Found")
        return False
    elif result.status_code == 200:
        return True
    else:
        agi.verbose('Error requesting the web service : %s' % result.text)
        return False

def main(agi):
    dst_num = agi.get_variable('XIVO_DSTNUM')
    if _is_recording_disabled(dst_num, agi):
        agi.set_variable('DO_RECORD', 'false')
    else:
        agi.set_variable('DO_RECORD', 'true')

def main():
    _get_auth_token(LOGIN_USER,MDP_USER)


if __name__ == '__main__':
    main()
    #main(AGI())
