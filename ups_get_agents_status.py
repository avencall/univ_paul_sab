#!/usr/bin/python
# -*- coding: UTF-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your OPTION) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
__version__   = '1.0'
__date__      = '20200521'
__author__    = 'Jean-Baptiste MARIN'

import sys
from xivo_auth_client import Client as Auth
from xivo_agentd_client import Client as Agentd
from xivo.agi import AGI
import requests
import json
from pprint import pprint

LOGIN_USER = "devspe"
MDP_USER = "devspe"
#IP_XIVO = "127.0.0.1"
IP_XIVO = "194.167.94.214"
CONFD_API_PORT = "9486"
AGENT_REST_URL = "https://"+IP_XIVO+":"+CONFD_API_PORT+"/1.1/agents?search="

requests.packages.urllib3.disable_warnings()
session = requests.Session()
session.verify = False
session.headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}



def _get_auth_token():
    auth = Auth(IP_XIVO, username=LOGIN_USER, password=MDP_USER, verify_certificate=False)
    token_data = auth.token.new('xivo_service',expiration=10)
    token = token_data['token']
    return(token)

def _is_agent_logged_in(dst_num,token):
    agentd = Agentd(IP_XIVO, username=LOGIN_USER, password=MDP_USER, verify_certificate=False, token=token)
    try:
        agent_status = agentd.agents.get_agent_status_by_number(dst_num).__dict__
        return(agent_status['logged'])
    except:
        return(False)

def _get_agent_group_by_number(dst_num,token,agi):
    URL=AGENT_REST_URL+dst_num
    #agi.verbose("URL TO QUERY %s :" % URL)
    #agi.verbose(" My token is %s :" % token)
    myToken = token
    session.headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Auth-Token': '{}'.format(myToken)}
    #head = {'X-Auth-Token : {}'.format(myToken)}
    agent_response = session.get(URL)
    #agi.verbose("response is %s :" % agent_response)
    if agent_response.status_code == 200:
            response=agent_response.json()
            #agi.verbose("response 2 is : %s " % response)
            if response['total'] == 1:
                #agi.verbose(response['items'])
                GROUP_NUM=response['items'][0]['numgroup']
    return(GROUP_NUM)


def main(agi):
    agi.verbose("Begin AGI")
    #dst_num = agi.get_variable('XIVO_DSTNUM')
    #agi.verbose("NUMBER IS %s" % (dst_num))
    dst_num = sys.argv[1]
    token = _get_auth_token()
    #dst_num = agi.get_variable('XIVO_DSTNUM')
    agi.verbose("NUMBER IS %s" % (dst_num))
    logged = _is_agent_logged_in(dst_num,token)
    agi.verbose("result query %s" % (logged))
    agi.set_variable('IS_AGENT_LOGGED_ON', '%s' % (logged))
    agi.verbose("Login status for agent number %s : %s" %  (dst_num,logged))
    num_group = _get_agent_group_by_number(dst_num,token,agi)
    agi.set_variable('GROUP_NUM', '%s' % (num_group))
    agi.verbose("NUMBER GROUP IS  %s" %  (num_group))

if __name__ == '__main__':
    main(AGI())
