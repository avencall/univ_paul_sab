#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import common

GROUPE_RENVOI_EXTERNE_PATTERN = 'renvoi_externe'


def ups_droits_renvoi(agi, cursor, args):
    try:
        exten = args[0]
    except IndexError:
        agi.dp_break('Extension manquante')
    userid = int(agi.get_variable('XIVO_USERID'))
    if(utilisateur_autorise(cursor, userid, exten)):
        agi.set_variable('utilisateur_autorise', 1)
    else:
        agi.set_variable('utilisateur_autorise', 0)


def utilisateur_autorise(cursor, userid, exten):
    if(exten.startswith('0')):
        return renvoi_externe_autorise(cursor, userid)
    else:
        return True


def renvoi_externe_autorise(cursor, userid):
    result = common.find_group_for_user_starting_with(cursor, userid, GROUPE_RENVOI_EXTERNE_PATTERN)
    return len(result) > 0

if(__name__ == '__main__'):
    common.with_agi_params(ups_droits_renvoi)
