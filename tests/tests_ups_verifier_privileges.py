# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
import ups_verifier_privileges
from common import db_connect
from mock import Mock, call
from helpers import Helper

ALLOW = 1
DENY = 0


class TestVerifierPrivileges(unittest.TestCase):

    def mock_get_variable(self, string):
        if(string == 'XIVO_USERID'):
            return self.userid
        elif(string == 'XIVO_DSTNUM'):
            return self.called_extension

    def setUp(self):
        self.connexion = db_connect('postgresql://asterisk:proformatique@localhost:5432/asterisk')
        self.cursor = self.connexion.cursor()
        self.helper = Helper(self.connexion, self.cursor)
        self.groupname = ups_verifier_privileges.GROUPE_PRIVILEGE_PATTERN + '_test'
        self.helper.create_group(self.groupname)
        self.userid = self.helper.create_user('Tartampion test', 'abcd')
        extensionid = self.helper.create_extension('2000')
        self.lineid = self.helper.create_line()
        self.helper.associate_user_to_extension(self.userid, extensionid, self.lineid)
        self.droitid = self.helper.create_droit_appel(ALLOW, 'aut_privilegies', ['**26.'])
        self.agi = Mock()
        self.agi.get_variable = self.mock_get_variable

    def tearDown(self):
        self.helper.delete_line(self.lineid)
        self.helper.delete_droit(self.droitid)
        self.cursor.close()

    def test_autorise(self):
        self.called_extension = '**260235487951'
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupname)
        self.helper.associer_groupe_au_droit(self.groupname, self.droitid)
        ups_verifier_privileges.ups_verifier_privileges(self.agi, self.cursor, ['2000'])
        self.assertEqual(self.agi.set_variable.call_args_list[0], call('mdp_utilisateur', 'abcd'))
        self.assertEqual(self.agi.set_variable.call_args_list[1], call('XIVO_AUTHORIZATION', 'ALLOW'))

    def test_non_autorise(self):
        ups_verifier_privileges.ups_verifier_privileges(self.agi, self.cursor, ['2000'])
        self.agi.set_variable.assert_called_with('XIVO_AUTHORIZATION', 'DENY')


if __name__ == "__main__":
    unittest.main()
