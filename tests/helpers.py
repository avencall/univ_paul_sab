# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

class Helper:
    def __init__(self, connexion, cursor):
        self.connexion = connexion
        self.cursor = cursor

    def create_group(self, nom):
        self.cursor.query("DELETE FROM queuemember WHERE queue_name = %s", None, (nom,))
        self.cursor.query("DELETE FROM groupfeatures WHERE name = %s", None, (nom,))
        self.cursor.query("INSERT INTO groupfeatures(name, context) VALUES (%s, 'default')", None, (nom,))
        self.connexion.commit()

    def create_user(self, nom, mdp):
        self.cursor.query("DELETE FROM user_line WHERE user_id IN (SELECT id FROM userfeatures WHERE lastname = %s)",
                          None, (nom,))
        self.cursor.query("DELETE FROM userfeatures WHERE lastname = %s", None, (nom,))
        self.cursor.query("INSERT INTO func_key_template(name, private) VALUES ('test', true) RETURNING ${columns}", ('id',))
        func_key_id = self.cursor.fetchone()['id']
        self.cursor.query("INSERT INTO userfeatures(firstname, lastname, passwdclient, description, func_key_private_template_id) VALUES ('', %s, %s, '', %s) " +
                          "RETURNING ${columns}", ('id',), (nom, mdp, func_key_id))
        userid = self.cursor.fetchone()['id']
        self.connexion.commit()
        return userid

    def ajouter_utilisateur_au_groupe(self, userid, nom_groupe):
        self.cursor.query("INSERT INTO queuemember(queue_name, interface, channel, usertype, userid, category) " +
                          "VALUES(%s, 'SIP/abcdefg', 'SIP', 'user', %s, 'group')",
                          None, (nom_groupe, userid))
        self.connexion.commit()

    def create_extension(self, numero):
        self.cursor.query("DELETE FROM user_line WHERE extension_id IN (SELECT id FROM extensions WHERE exten = %s)", None, (numero,))
        self.cursor.query("DELETE FROM extensions WHERE exten = %s", None, (numero,))
        self.cursor.query("INSERT INTO extensions(context, exten, type) VALUES('default', %s, 'user')" +
                          "RETURNING ${columns}", ('id',), (numero,))
        extenid = self.cursor.fetchone()['id']
        self.connexion.commit()
        return extenid

    def create_line(self):
        name = '1234ad'
        self.cursor.query("DELETE FROM linefeatures WHERE name=%s", None, (name,))
        self.cursor.query("SELECT max(protocolid) AS ${columns} FROM linefeatures", ('protoid',), None)
        protoid = self.cursor.fetchone()['protoid'] + 1
        self.cursor.query("INSERT INTO linefeatures(protocol, protocolid, name, context, provisioningid)" +
                          "VALUES ('sip', %s, %s, 'default', '789456') RETURNING ${columns}",
                          ('id',), (protoid, name))
        lineid = self.cursor.fetchone()['id']
        self.connexion.commit()
        return lineid

    def associate_user_to_extension(self, userid, extensionid, lineid):
        self.cursor.query("DELETE FROM user_line WHERE user_id = %s AND extension_id = %s AND line_id = %s",
                          None, (userid, extensionid, lineid))
        self.cursor.query("INSERT INTO user_line(user_id, line_id, extension_id, main_line, main_user) " +
                          "VALUES (%s, %s, %s, true, true)", None, (userid, lineid, extensionid))
        self.connexion.commit()

    def delete_line(self, lineid):
        self.cursor.query("DELETE FROM user_line WHERE line_id = %s", None, (lineid,))
        self.cursor.query("DELETE FROM linefeatures WHERE id = %s", None, (lineid,))
        self.connexion.commit()

    def delete_droit(self, droitid):
        self.cursor.query("DELETE FROM rightcallexten WHERE rightcallid = %s",
                          None, (droitid,))
        self.cursor.query("DELETE FROM rightcallmember WHERE rightcallid = %s",
                          None, (droitid,))
        self.cursor.query("DELETE FROM rightcall WHERE id = %s",
                          None, (droitid,))
        self.connexion.commit()

    def create_droit_appel(self, autorisation, nom, extensions):
        self.cursor.query("DELETE FROM rightcall WHERE name = %s", None, (nom,))
        self.cursor.query("INSERT INTO rightcall(name, \"authorization\", description) VALUES(%s, %s, '') RETURNING ${columns}",
                          ('id',), (nom, autorisation))
        droitid = self.cursor.fetchone()['id']
        for extension in extensions:
            self.cursor.query("INSERT INTO rightcallexten(rightcallid, exten) VALUES (%s, %s)",
                              None, (droitid, extension))
        self.connexion.commit()
        return droitid

    def associer_groupe_au_droit(self, nom_groupe, droitid):
        self.cursor.query("SELECT ${columns} FROM groupfeatures WHERE name = %s", ('id',), (nom_groupe,))
        groupeid = self.cursor.fetchone()['id']
        self.cursor.query("INSERT INTO rightcallmember(rightcallid, type, typeval) VALUES (%s, 'group', %s)",
                           None, (droitid, str(groupeid)))
        self.connexion.commit()

    def creer_horaire(self, nom, heure_debut, heure_fin):
        self.cursor.query("DELETE FROM schedule_path WHERE schedule_id IN (SELECT id FROM schedule WHERE name = %s)",
                          None, (nom,))
        self.cursor.query("DELETE FROM schedule_time WHERE schedule_id IN (SELECT id FROM schedule WHERE name = %s)",
                          None, (nom,))
        self.cursor.query("DELETE FROM schedule WHERE name = %s", None, (nom,))
        self.cursor.query("INSERT INTO schedule(name, timezone) VALUES (%s, 'Europe/Paris') RETURNING ${columns}", ('id',), (nom,))
        horaireid = self.cursor.fetchone()['id']
        hours = '%s-%s' % (heure_debut, heure_fin)
        self.cursor.query("INSERT INTO schedule_time(schedule_id, hours) VALUES(%s, %s)",
                          None, (horaireid, hours))
        self.connexion.commit()

    def associer_groupe_a_horaire(self, nom_groupe, nom_horaire):
        self.cursor.query("SELECT ${columns} FROM groupfeatures WHERE name = %s", ('id',), (nom_groupe,))
        groupid = self.cursor.fetchone()['id']
        self.cursor.query("SELECT ${columns} FROM schedule WHERE name = %s", ('id',), (nom_horaire,))
        horaireid = self.cursor.fetchone()['id']
        self.cursor.query("INSERT INTO schedule_path(schedule_id, path, pathid, \"order\") VALUES (%s, 'group', %s, 1)",
                          None, (horaireid, groupid))
