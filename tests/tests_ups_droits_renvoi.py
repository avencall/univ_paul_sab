# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
import ups_droits_renvoi
from common import db_connect
from mock import Mock
from helpers import Helper


class TestDroitsRenvoi(unittest.TestCase):

    def mock_get_variable(self, string):
        if(string == 'XIVO_USERID'):
            return self.userid

    def setUp(self):
        self.connexion = db_connect('postgresql://asterisk:proformatique@localhost:5432/asterisk')
        self.cursor = self.connexion.cursor()
        self.helper = Helper(self.connexion, self.cursor)
        self.groupename = ups_droits_renvoi.GROUPE_RENVOI_EXTERNE_PATTERN + '_test'
        self.helper.create_group('renvoi_externe_test')
        self.userid = self.helper.create_user('Tartampion test', self.groupename)
        self.agi = Mock()
        self.agi.get_variable = self.mock_get_variable

    def tearDown(self):
        self.cursor.close()

    def test_renvoi_interne(self):
        ups_droits_renvoi.ups_droits_renvoi(self.agi, self.cursor, ['5000'])
        self.agi.set_variable.assert_called_with('utilisateur_autorise', 1)

    def test_renvoi_externe_interdit(self):
        ups_droits_renvoi.ups_droits_renvoi(self.agi, self.cursor, ['0231548791'])
        self.agi.set_variable.assert_called_with('utilisateur_autorise', 0)

    def test_renvoi_externe_autorise(self):
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupename)
        ups_droits_renvoi.ups_droits_renvoi(self.agi, self.cursor, ['0231548791'])
        self.agi.set_variable.assert_called_with('utilisateur_autorise', 1)

if __name__ == "__main__":
    unittest.main()
