# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
from common import db_connect
from helpers import Helper
from mock import Mock, call
import set_mdp


class TestSetMdp(unittest.TestCase):

    def mock_get_variable(self, string):
        if(string == 'XIVO_USERID'):
            return self.userid

    def setUp(self):
        self.connexion = db_connect('postgresql://asterisk:proformatique@localhost:5432/asterisk')
        self.cursor = self.connexion.cursor()
        self.helper = Helper(self.connexion, self.cursor)
        self.userid = self.helper.create_user('Tartampion test', 'abcd')
        self.agi = Mock()
        self.agi.get_variable = self.mock_get_variable

    def test_mdp(self):
        set_mdp.set_mdp(self.agi, self.cursor, ['mdp'])
        self.assertEqual(self.agi.set_variable.call_args_list[0], call('mdp_utilisateur', 'abcd'))
