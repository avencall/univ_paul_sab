# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest
import ups_droits_jour_nuit
from common import db_connect
from mock import Mock, call, patch
from helpers import Helper
from datetime import datetime

ALLOW = 1
DENY = 0


class TestDroitsJourNuit(unittest.TestCase):

    def mock_get_variable(self, string):
        if(string == 'XIVO_USERID'):
            return self.userid
        if string == 'XIVO_DSTNUM':
            return self.dstnum

    def setUp(self):
        self.connexion = db_connect('postgresql://asterisk:proformatique@localhost:5432/asterisk')
        self.cursor = self.connexion.cursor()
        self.helper = Helper(self.connexion, self.cursor)
        self.userid = self.helper.create_user('Tartampion test', 'abcd')
        self.groupejour_name = ups_droits_jour_nuit.GROUPE_JOUR_PATTERN + '_test'
        self.groupenuit_name = ups_droits_jour_nuit.GROUPE_NUIT_PATTERN + '_test'
        self.groupehoraire_name = ups_droits_jour_nuit.GROUPE_HORAIRE_PATTERN + '_test'
        self.helper.create_group(self.groupejour_name)
        self.helper.create_group(self.groupenuit_name)
        self.helper.create_group(self.groupehoraire_name)
        self.droitjour = self.helper.create_droit_appel(ALLOW, 'droit_jour', ['**26.', '**27.'])
        self.droitnuit = self.helper.create_droit_appel(ALLOW, 'droit_nuit', ['**26.'])
        self.helper.creer_horaire('mon_horaire_test', '09:00', '18:00')
        self.helper.associer_groupe_a_horaire(self.groupehoraire_name, 'mon_horaire_test')
        self.helper.associer_groupe_au_droit(self.groupejour_name, self.droitjour)
        self.helper.associer_groupe_au_droit(self.groupenuit_name, self.droitnuit)
        self.agi = Mock()
        self.agi.get_variable = self.mock_get_variable

    def tearDown(self):
        self.helper.delete_droit(self.droitjour)
        self.helper.delete_droit(self.droitnuit)
        self.cursor.close()

    @patch('xivo_agid.schedule.Schedule._get_current_localized_time')
    def test_autorise_jour(self, patch_now):
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupejour_name)
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupehoraire_name)
        self.dstnum = '**275210'
        patch_now.return_value = datetime(year=2013, month=12, day=14, hour=10, minute=30)

        ups_droits_jour_nuit.ups_droits_jour_nuit(self.agi, self.cursor, ['autorisation'])

        self.assertEqual(self.agi.set_variable.call_args_list[0], call('XIVO_AUTHORIZATION', 'ALLOW'))

    @patch('xivo_agid.schedule.Schedule._get_current_localized_time')
    def test_interdit_jour_car_pas_de_groupe(self, patch_now):
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupehoraire_name)
        self.dstnum = '**275210'
        patch_now.return_value = datetime(year=2013, month=12, day=14, hour=10, minute=30)

        ups_droits_jour_nuit.ups_droits_jour_nuit(self.agi, self.cursor, ['autorisation'])

        self.assertEqual(self.agi.set_variable.call_args_list[0], call('XIVO_AUTHORIZATION', 'DENY'))

    @patch('xivo_agid.schedule.Schedule._get_current_localized_time')
    def test_interdit_jour_car_pas_de_droit(self, patch_now):
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupejour_name)
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupehoraire_name)
        self.dstnum = '**285210'
        patch_now.return_value = datetime(year=2013, month=12, day=14, hour=10, minute=30)

        ups_droits_jour_nuit.ups_droits_jour_nuit(self.agi, self.cursor, ['autorisation'])

        self.assertEqual(self.agi.set_variable.call_args_list[0], call('XIVO_AUTHORIZATION', 'DENY'))

    @patch('xivo_agid.schedule.Schedule._get_current_localized_time')
    def test_autorise_nuit(self, patch_now):
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupenuit_name)
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupehoraire_name)
        self.dstnum = '**265210'
        patch_now.return_value = datetime(year=2013, month=12, day=14, hour=19, minute=30)

        ups_droits_jour_nuit.ups_droits_jour_nuit(self.agi, self.cursor, ['autorisation'])

        self.assertEqual(self.agi.set_variable.call_args_list[0], call('XIVO_AUTHORIZATION', 'ALLOW'))

    @patch('xivo_agid.schedule.Schedule._get_current_localized_time')
    def test_interdit_nuit_car_pas_de_groupe(self, patch_now):
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupehoraire_name)
        self.dstnum = '**275210'
        patch_now.return_value = datetime(year=2013, month=12, day=14, hour=19, minute=30)

        ups_droits_jour_nuit.ups_droits_jour_nuit(self.agi, self.cursor, ['autorisation'])

        self.assertEqual(self.agi.set_variable.call_args_list[0], call('XIVO_AUTHORIZATION', 'DENY'))

    @patch('xivo_agid.schedule.Schedule._get_current_localized_time')
    def test_interdit_nuit_car_pas_de_droit(self, patch_now):
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupenuit_name)
        self.helper.ajouter_utilisateur_au_groupe(self.userid, self.groupehoraire_name)
        self.dstnum = '**285210'
        patch_now.return_value = datetime(year=2013, month=12, day=14, hour=19, minute=30)

        ups_droits_jour_nuit.ups_droits_jour_nuit(self.agi, self.cursor, ['autorisation'])

        self.assertEqual(self.agi.set_variable.call_args_list[0], call('XIVO_AUTHORIZATION', 'DENY'))

if __name__ == "__main__":
    unittest.main()
