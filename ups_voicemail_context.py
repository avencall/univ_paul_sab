#!/usr/bin/python

import requests
import json
import sys
from xivo.agi import AGI
import pprint
agi = AGI()

SERVER = "https://localhost:9486/1.1"
session = requests.Session()
session.verify = False
session.headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}


def get_voicemail_context_by_number( num ):
    try:
        agi.verbose(num)
	url = "{}/voicemails".format(SERVER)
	query = {'search': num}
	response = session.get(url, params=query).json()
	pprint(response)
	agi.verbose(response)
    except Exception, e:
        agi.verbose(str(e))
    return response['items'][0]

try:
	numero = agi.get_variable('UPS_NUMERO_INTERNE')
	eresult = get_voicemail_context_by_number(str(numero))
	
	context = eresult['context']
	number  = eresult['number']
	
	agi.set_variable('XIVO_MAILBOX', number)
	agi.set_variable('XIVO_MAILBOX_CONTEXT', context)
except Exception, e:
    agi.verbose(str(e))
