#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import common

def set_mdp(agi, cursor, args):
    userid = agi.get_variable('XIVO_USERID')
    agi.set_variable('mdp_utilisateur', common.get_mdp_utilisateur(cursor, userid))

if(__name__ == '__main__'):
    common.with_agi_params(set_mdp)
