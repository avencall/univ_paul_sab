#!/usr/bin/python
# -*- coding: UTF-8 -*-
# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,

import argparse
import logging
import sys
import traceback
import requests

from xivo.agi import AGI

#
# User configuration
# -------------

DEBUG_MODE = True # False 
LOGFILE = '/var/log/asterisk/xivo-routage-agi.log'

# By the order of importance, the most important first
#WS_IP = ['194.167.94.200','194.167.94.201']
WS_IP = ['194.167.94.212','194.167.94.213']

#
# Factory configuration
# -------------

URL_HEADERS = { 'User-Agent' : 'XiVO Webservice AGI' }

# Without server address/name
REQUEST_URL = 'route'
PARAMETER_NAME = 'digits'

# timeout of the tcp connection to the webserver (seconds)
CONNECTION_TIMEOUT = 2

agi = AGI()
logger = logging.getLogger()


class Syslogger(object):

    def write(self, data):
        global logger
        logger.error(data)


def init_logging(debug_mode):
    if debug_mode:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logfilehandler = logging.FileHandler(LOGFILE)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logfilehandler.setFormatter(formatter)
    logger.addHandler(logfilehandler)

    syslogger = Syslogger()
    sys.stderr = syslogger


def get_routes(digits):
    params = {PARAMETER_NAME: digits}

    i = 0
    get_routes_request = None
    for ip in WS_IP:
        logger.debug("Getting routes for " + str(digits) + " from " + ip)
        get_routes_request = None
        try:
            get_routes_request = requests.get("http://" + ip + "/" + REQUEST_URL,
                                              params=params,
                                              timeout=CONNECTION_TIMEOUT)

        except requests.exceptions.ConnectionError:
            logger.warning("Server " + ip + " refused connection")
            ups_error_cause = "Refused connection from WS: " + ip

        except ValueError:
            logger.error("No JSON encoded response from " + ip)
            ups_error_cause = "No JSON encoded response from " + ip

        except requests.exceptions.Timeout:
            logger.error("Timeout when connecting to the following server " + ip)
            ups_error_cause = "Timeout when connecting to the following server " + ip

        except:
            logger.error("Unknown problem during connection to the following server: " + ip)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.debug(repr(traceback.format_exception(exc_type, exc_value,
                                              exc_traceback)))
            ups_error_cause = "Connection failed to WS"
        if get_routes_request != None:
            if get_routes_request.status_code != requests.codes.ok:
                if get_routes_request.status_code == requests.codes.not_found:
                    ups_error_cause = "No entry found for " + str(digits) + " on server " + ip
                    logger.warning("No entry found for " + str(digits) + " on server " + ip)
                    process_no_response(digits, ups_error_cause)
                else:
                    logger.error("Server " + ip + " returned following error: " + str(get_routes_request.status_code))
                    ups_error_cause = "Server " + ip + " returned following error: " + str(get_routes_request.status_code)

            else:
                routes = get_routes_request.json()
                logger.info("Got following routes for digits: " + str(digits) + ": " + str(routes))
                return routes
        else:
            if ups_error_cause == None:
                ups_error_cause = "Unknown problem during connection to the following server: " + ip
                logger.error("Unknown problem during connection to the following server: " + ip)


    process_no_response(digits, ups_error_cause)

#agi.set_variable("__VARNAME", routes[0]) #routes['regexp']
def set_dialplan_variables(routes):
    logger.debug("Setting dialplan variables using: " + str(routes))
    agi.set_variable("ups_digits", routes['digits'])
    agi.set_variable("ups_regexp", routes['regexp'])
    agi.set_variable("ups_target", routes['target'])
    agi.set_variable("ups_context", routes['context'])
    #agi.set_variable("ups_subroutine", routes['subroutine'])

#Timeout & tous les cas ou on n'a pas de réponse
def process_no_response(digits, ups_error_cause):
    logger.warning("Got no response for digits: " + str(digits) + "!")
    agi.set_variable("ups_error_cause", ups_error_cause)
    sys.exit(1)

def process_request(digits):
    routes = get_routes(digits)
    set_dialplan_variables(routes)


def main():
    init_logging(DEBUG_MODE)
    try:
        if len(sys.argv) < 1:
            logger.error("wrong number of arguments")
            sys.exit(1)
        process_request(sys.argv[1])
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.error(repr(traceback.format_exception(exc_type, exc_value,
                                          exc_traceback)))
        sys.exit(1)

    sys.exit(0)


if __name__ == '__main__':
    main()
