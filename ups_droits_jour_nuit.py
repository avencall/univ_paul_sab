#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_agid import call_rights
import common
from xivo_agid import objects

GROUPE_HORAIRE_PATTERN = 'horaire'
GROUPE_JOUR_PATTERN = 'jour'
GROUPE_NUIT_PATTERN = 'nuit'
DROIT_NUM_INTERDITS = 'numero-interdits'


def ups_droits_jour_nuit(agi, cursor, args):
    try:
        _ups_droits_jour_nuit(agi, cursor)
    except call_rights.RuleAppliedException:
        return


# check rightcall for the calling user
def _ups_droits_jour_nuit(agi, cursor):
    userid = agi.get_variable('XIVO_USERID')
    dstnum = agi.get_variable('XIVO_DSTNUM')
    ups_anonyme = agi.get_variable('UPS_ANONYME')
    if ups_anonyme == '1':
        dstnum = "0" + dstnum[3:]
    if dstnum[:3] == "+33":
        dstnum="00"+dstnum[3:]

    agi.verbose("### Specific Rigthcall: Dstnum is %s" %(dstnum))
    # check if the number is forbidden
    if common.is_exten_contained_in_rightcall(cursor, dstnum, DROIT_NUM_INTERDITS):
        agi.verbose("### Specific Rigthcall: This number is forbidden, check the right call %s" % DROIT_NUM_INTERDITS)
        call_rights.deny(agi, None)
    agi.verbose("### Specific Rigthcall: this number is allowed %s" %(dstnum))    
    
    # get groups like horaire_<...> where the user is member
    ids_groupes_horaire = trouver_groupe_horaire(agi, cursor, userid)
    if not ids_groupes_horaire:
            agi.verbose("### Specific Rigthcall: user not member of group <entity>_horaire_")
            call_rights.deny(agi, None)
    agi.verbose("### Specific Rigthcall: user is member of groupe like horaire ")

    # check if there is a schedule on this groups
    cursor.query('''SELECT ${columns} FROM schedule_time st
                    NATURAL JOIN schedule_path sp
                    WHERE sp.path = 'group' AND sp.pathid = %s''',
                    ('st.hours',),
                    (ids_groupes_horaire[0],))
    horaires = cursor.fetchall()

    # if there is not horaire, the call is deny
    if not horaires:
        agi.verbose("### Specific Rigthcall: not sckedule for the groupe where the user is member")
        call_rights.deny(agi, None)
    agi.verbose("### Specific Rigthcall: the group horaire have a schedule")

    # agi.verbose("debug 1 %s"(ids_groupes_horaire[0]))
    path = 'group'
    path_id = ids_groupes_horaire[0]

    # check if the schedule is opened or closed
    schedule = objects.ScheduleDataMapper.get_from_path(cursor, path, path_id)
    schedule_state = schedule.compute_state_for_now()
    agi.verbose("### Specific Rigthcall: Schedule for group is %s" % (schedule_state.state))
    if schedule_state.state == 'closed':
        groupids = trouver_groupes_nuit(cursor, userid)
    else:
        groupids = trouver_groupes_jour(cursor, userid)

    if not groupids:
        call_rights.deny(agi, None)

    # check if the exten is authorized is the rightcall
    rightcallids = common.get_rightcalls_matching_exten(cursor, dstnum)
    if not rightcallids:
        call_rights.deny(agi, None)
    agi.verbose("### Specific Rigthcall: user is allowed to make call on %s" %(dstnum))

    res = common.get_rightcall_elements_for_rightcalls_and_groups(cursor, rightcallids, groupids)
    call_rights.apply_rules(agi, res)
    call_rights.deny(agi)


def trouver_groupe_horaire(agi, cursor, userid):
    return common.find_group_for_user_starting_with(cursor, userid, GROUPE_HORAIRE_PATTERN)


def trouver_groupes_jour(cursor, userid):
    return common.find_group_for_user_starting_with(cursor, userid, GROUPE_JOUR_PATTERN)


def trouver_groupes_nuit(cursor, userid):
    return common.find_group_for_user_starting_with(cursor, userid, GROUPE_NUIT_PATTERN)

if(__name__ == '__main__'):
    common.with_agi_params(ups_droits_jour_nuit)
