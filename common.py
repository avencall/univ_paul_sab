# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_agid import call_rights
from xivo import anysql
from xivo.BackSQL import backpostgresql
from xivo.agi import AGI
import sys

DB_URI = 'postgresql://asterisk:proformatique@localhost:5432/asterisk'


def db_connect(db_uri):
    return anysql.connect_by_uri(db_uri)


def get_mdp_utilisateur(cursor, userid):
    cursor.query('SELECT ${columns} FROM userfeatures WHERE id = %s',
                 ('passwdclient',),
                 (userid,))
    result = cursor.fetchall()
    return result[0]['passwdclient']


def find_group_for_user_starting_with(cursor, userid, prefix):
    cursor.query('''SELECT ${columns} FROM groupfeatures g
                    JOIN queuemember qm ON g.name = qm.queue_name
                    WHERE qm.usertype='user' AND qm.userid=%s AND qm.queue_name LIKE %s ''',
                    ('g.id',),
                    (userid, '%'+prefix + '%'))
    result = cursor.fetchall()
    return [line['g.id'] for line in result]


def get_rightcalls_matching_exten(cursor, dstnum):
    cursor.query("SELECT ${columns} FROM rightcallexten",
                 ('rightcallid', 'exten'))
    res = cursor.fetchall()

    rightcallidset = set((row['rightcallid'] for row in res if call_rights.extension_matches(dstnum, row['exten'])))

    return [str(item) for item in rightcallidset]


def is_exten_contained_in_rightcall(cursor, dstnum, rightcall_name):
    cursor.query("SELECT ${columns} FROM rightcall r JOIN rightcallexten re ON r.id=re.rightcallid WHERE r.name= %s",
                 ('exten',), (rightcall_name,))
    res = cursor.fetchall()
    for row in res:
        if call_rights.extension_matches(dstnum, row['exten']):
            return True
    return False


def is_user_contained_in_rightcall(cursor, user_id, rightcall_name):
    cursor.query("SELECT ${columns} FROM rightcallmember where type = 'user' and typeval = '%s';",
                 (user_id,), (rightcall_name,))
    res = cursor.fetchall()
    for row in res:
        if user_id == row['user_id']:
            return True
    return False



def get_rightcall_elements_for_rightcalls_and_groups(cursor, rightcallids, groupids):
    cursor.query("SELECT ${columns} FROM rightcall "
                 "INNER JOIN rightcallmember "
                 "ON rightcall.id = rightcallmember.rightcallid "
                 "WHERE rightcall.id IN (" + ", ".join(rightcallids) + ") "
                 "AND rightcallmember.type = 'group' "
                 "AND rightcallmember.typeval IN (" + ", ".join(["'%s'"] * len(groupids)) + ") "
                 "AND rightcall.commented = 0",
                 (call_rights.RIGHTCALL_AUTHORIZATION_COLNAME, call_rights.RIGHTCALL_PASSWD_COLNAME),
                 groupids)
    return cursor.fetchall()


def with_agi_params(fonction):
    connexion = db_connect(DB_URI)
    cursor = connexion.cursor()
    fonction(AGI(), cursor, sys.argv[1:])
    cursor.close()
    connexion.close()
