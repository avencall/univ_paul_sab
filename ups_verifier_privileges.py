#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2011-2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_agid import call_rights
import common

GROUPE_PRIVILEGE_PATTERN = 'privilege'


def ups_verifier_privileges(agi, cursor, args):
    try:
        _ups_verifier_privileges(agi, cursor, args)
    except call_rights.RuleAppliedException:
        return


def _ups_verifier_privileges(agi, cursor, args):
    try:
        exten = args[0]
    except IndexError:
        agi.dp_break('Extension manquante')

    userid = userid_from_exten(cursor, exten)
    if not userid:
        call_rights.deny(agi, None)
    user_mdp = common.get_mdp_utilisateur(cursor, userid)
    if user_mdp == "":
        call_rights.deny(agi, None)
    agi.set_variable('mdp_utilisateur', common.get_mdp_utilisateur(cursor, userid))

    groupids = common.find_group_for_user_starting_with(cursor, userid, GROUPE_PRIVILEGE_PATTERN)
    if not groupids:
        call_rights.deny(agi, None)

    dstnum = agi.get_variable('XIVO_DSTNUM')
    rightcallids = common.get_rightcalls_matching_exten(cursor, dstnum)
    if not rightcallids:
        call_rights.deny(agi, None)

    res = common.get_rightcall_elements_for_rightcalls_and_groups(cursor, rightcallids, groupids)
    call_rights.apply_rules(agi, res)
    call_rights.deny(agi, None)


def userid_from_exten(cursor, exten):
    cursor.query('''SELECT ${columns} FROM user_line ul
                    JOIN extensions e ON e.id = ul.extension_id
                    WHERE e.exten = %s''',
                    ('ul.user_id',),
                    (exten,))
    res = cursor.fetchall()
    if res:
        return res[0]['ul.user_id']
    else:
        return None

if(__name__ == '__main__'):
    common.with_agi_params(ups_verifier_privileges)
