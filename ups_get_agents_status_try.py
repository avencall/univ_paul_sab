#!/usr/bin/python
# -*- coding: UTF-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your OPTION) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
__version__   = '1.0'
__date__      = '20200521'
__author__    = 'Jean-Baptiste MARIN'

import sys
from xivo_auth_client import Client as Auth
from xivo_agentd_client import Client as Agentd
import requests
import json
from pprint import pprint
from xivo.agi import AGI


LOGIN_USER = "devspe"
MDP_USER = "devspe"
IP_XIVO = "127.0.0.1"
#IP_XIVO = "194.167.94.214"
CONFD_API_PORT = "9486"
AGENT_REST_URL = "https://"+IP_XIVO+":"+CONFD_API_PORT+"/1.1/agents/"

#requests.packages.urllib3.disable_warnings()
#session = requests.Session()
#session.verify = False
#session.headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}



def _get_auth_token():
    auth = Auth(IP_XIVO, username=LOGIN_USER, password=MDP_USER, verify_certificate=False)
    token_data = auth.token.new('xivo_service',expiration=10)
    token = token_data['token']
    return(token)

def _is_agent_logged_in(dst_num,token,agi):
    agi.verbose("agentd begin")
    agentd = Agentd(IP_XIVO, username=LOGIN_USER, password=MDP_USER, verify_certificate=False, token=token)
    agi.verbose("token is %s " % (agentd))
    #confd = Confd(IP_XIVO, port=CONFD_API_PORT, verify_certificate=False, token=token)
    agent_status = agentd.agents.get_agent_status_by_number(dst_num).__dict__
    agi.verbose("%s" % (agent_status))
    #agent_id = agent_status['id']
    logged = agent_status['logged']
    return(logged)
    #except:
    #    return(False)

def _get_agent_group_by_id(agent_id,token,agi):
    URL=AGENT_REST_URL+agent_id
    myToken = token
    head = {'Authorization': 'token {}'.format(myToken)}
    agent_response = session.get(URL,head)
    agi.verbose(agent_response)
    if agent_response.status_code == 200:
            response=agent_response.json()
            agi.verbose(response)
            if response['total'] == 1:
                agi.verbose(response['items'])
                GROUP_NUM=response['items'][0]['numgroup']
    return(GROUP_NUM)

def main(agi):
    agi.verbose("Begin AGI")
    #dst_num = agi.get_variable('XIVO_DSTNUM')
    #agi.verbose("NUMBER IS %s" % (dst_num))
    dst_num = sys.argv[1]
    agi.verbose("NUMBER IS %s" % (dst_num))
    #query = _is_agent_logged_in(dst_num,_get_auth_token())
    #agi.verbose(query)
    query = _is_agent_logged_in(dst_num,_get_auth_token(),agi)
    #agent_id = query.agent_id
    agi.verbose("query is %s" % (query))
    #logged = query[0]
    #agent_id = query[1]
    agi.verbose("result query %s for agent_id  %s" % (logged,agent_id))
    agi.set_variable('IS_AGENT_LOGGED_ON', '%s' % (logged))
    agi.verbose("Login status for agent number %s : %s" %  (dst_num,logged))
    num_group = _get_agent_group_by_id(agent_id,_get_auth_token(),agi)
    agi.set_variable('NUM_GROUP', '%s' % (num_group))
    agi.verbose("NUMBER GROUP IS  %s" %  (num_group))

if __name__ == '__main__':
    main(AGI())
